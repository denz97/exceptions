package src;

public class Main {

    public static void main(String[] args) {
        try {
            Exception.checkAge(17);
        } catch (ArithmeticException dog) {

            System.out.println("Catch");
        } finally {
            System.out.println("Executed after catch");
        }

    }
}

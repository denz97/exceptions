package src;

public class Exception {

    static void checkAge(int age) {
        if (age < 18) {
            System.out.println("False");
            throw new ArithmeticException("False");
        } else {
            System.out.println("True");
        }
    }

    void method() throws InterruptedException {
        Thread.sleep(10);
    }
}
